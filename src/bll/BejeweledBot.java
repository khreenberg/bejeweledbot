package bll;

import java.awt.Point;
import java.awt.Robot;
import java.awt.event.InputEvent;

import dal.ScreenReader;

import bll.states.StateManager;

public class BejeweledBot{

    public static final int INSTRUCTION_DELAY = 5;

    private Robot bot;

    private ScreenReader scrReader;

    private static BejeweledBot instance;

    private BejeweledBot() {
        try{
            bot = new Robot();
            scrReader = ScreenReader.getInstance();
            StateManager.getInstance().init( this );
        }
        catch( Exception e ){
            // TODO: handle exception
            e.printStackTrace();
        }
    }

    public static BejeweledBot getInstance(){
        if( instance == null ) instance = new BejeweledBot();
        return instance;
    }

    public void update(){
        StateManager.getInstance().updateState( scrReader.getAppScreen() );
        makeMove();
    }

    private void makeMove(){
        StateManager.getInstance().getState().act();
    }

    public void moveMouse( int newX, int newY ){
        bot.mouseMove( newX, newY );
        bot.delay( INSTRUCTION_DELAY );
    }

    public void delay( int ms ){
        bot.delay( ms );
    }

    public void typeKey( int keyCode ){
        bot.keyPress( keyCode );
        bot.delay( INSTRUCTION_DELAY );
        bot.keyRelease( keyCode );
    }

    public void mousePress( int button ){
        bot.mousePress( button );
    }

    public void mouseRelease( int button ){
        bot.mouseRelease( button );
    }

    public void mouseClick( int button ){
        mousePress( button );
        mouseRelease( button );
        delay( INSTRUCTION_DELAY );
    }

    public void clickCoordinate( Point p ){
        moveMouse( p.x, p.y );
        mouseClick( InputEvent.BUTTON1_DOWN_MASK );
    }

    public Robot getBot(){
        return bot;
    }
}
