package bll.states;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.BejeweledBot;
import dal.ScreenReader;

public abstract class State{

    private String stateName;
    protected ScreenReader scrReader;
    protected BejeweledBot bot;

    protected HashMap<Point, Color> stateIdentifiers;

    protected State( String name ) {
        try{
            this.scrReader = ScreenReader.getInstance();
            this.stateName = name;
            setStateIdentifiers();
        }
        catch( AWTException e ){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public abstract void act();

    public abstract void setStateIdentifiers();

    @SuppressWarnings( "unused" )
    public void enter( State oldState ){
        // Do nothing by default
    }

    @SuppressWarnings( "unused" )
    public void leave( State newState ){
        // Do nothing by default
    }

    public HashMap<Point, Color> getIdentifiers(){
        return stateIdentifiers;
    }

    public void registerBot( BejeweledBot bot ){
        this.bot = bot;
    }

    @Override
    public String toString(){
        return stateName;
    }
}
