package bll.states.interplay;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class PostPlayState extends State{

    public PostPlayState() {
        super( "PostPlayState" );
    }

    private static final Point PLAY_AGAIN = new Point( 270, 375 );

    @Override
    public void act(){
        
        bot.clickCoordinate( scrReader.convertAppPositionToScreen( PLAY_AGAIN ) );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){
            {
                put( new Point( 25, 135 ), new Color( 0xce7a3a ) );
                put( new Point( 500, 315 ), new Color( 0xa4521f ) );
            }
        };
    }
}
