package bll.states.interplay;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class SpecialOfferState extends State{

    public SpecialOfferState( ) {
        super( "SpecialOfferState" );
    }

    @SuppressWarnings( "unused" )
    private static final Point HARVEST = new Point( 330, 440 );
    private static final Point NO_THANKS = new Point( 210, 440 );
    
    @Override
    public void act(){
        bot.clickCoordinate( scrReader.convertAppPositionToScreen( NO_THANKS ) );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){
            {
                put( new Point( 45, 425 ), new Color( 0x982d54 ) );
                put( new Point( 460, 111 ), new Color( 0xcb4a63 ) );
            }
        };
    }
}
