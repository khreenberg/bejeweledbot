package bll.states.interplay;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class PrePlayState extends State{

    public PrePlayState( ) {
        super( "PrePlayState" );
    }

    private static final Point PLAY_NOW = new Point( 270, 408 );
    
    @Override
    public void act(){
        bot.clickCoordinate( scrReader.convertAppPositionToScreen( PLAY_NOW ) );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){{
            put( new Point( 140, 110 ), new Color( 0xb56e2a ));
            put( new Point( 80, 410 ), new Color( 0xbc7531 ));
        }};
    }
}