package bll.states.interplay;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class LevelUpState extends State{

    @SuppressWarnings( "unused" )
    private static final Point CLAIM_NOW = new Point( 200, 400 );
    private static final Point CONTINUE = new Point( 345, 400 );

    public LevelUpState() {
        super( "LevelUpState" );
    }

    @Override
    public void act(){
        bot.clickCoordinate( scrReader.convertAppPositionToScreen( CONTINUE ) );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){
            {
                put( new Point( 93, 111 ), new Color( 0x5b0e40 ) );
                put( new Point( 441, 414 ), new Color( 0xffff99 ) );
            }
        };
    }

}
