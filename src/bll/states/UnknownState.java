package bll.states;

import java.util.HashMap;

public class UnknownState extends State{

    protected UnknownState() {
        super( "Unknown State" );
    }

    @Override
    public void act(){
        // Do nothing
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<>();
    }

}
