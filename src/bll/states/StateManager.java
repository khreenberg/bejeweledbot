package bll.states;

import static tools.ColorCompare.*;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Map.Entry;
import java.util.Set;

import bll.BejeweledBot;
import bll.states.interplay.LevelUpState;
import bll.states.interplay.PostPlayState;
import bll.states.interplay.PrePlayState;
import bll.states.interplay.SpecialOfferState;
import bll.states.play.PlayState;
import bll.states.play.SpecialGemState;
import bll.states.play.TimeUpState;

public class StateManager{

    // @foff
    private State currentState;
    private final UnknownState unknownState = new UnknownState();
    private final State[] STATES = { new PrePlayState(),
                                     new PlayState(),
                                     new SpecialGemState(),
                                     new TimeUpState(),
                                     new LevelUpState(),
                                     new PostPlayState(),
                                     new SpecialOfferState() };
    // @fon
    private static StateManager instance;

    private StateManager() {
        currentState = STATES[0];
    }

    public static StateManager getInstance(){
        if( instance == null ) instance = new StateManager();
        return instance;
    }

    public void init( BejeweledBot bot ){
        for( State state : STATES )
            state.registerBot( bot );
    }

    public State getState(){
        return currentState;
    }

    public void updateState( BufferedImage img ){
        for( State state : STATES ){
            if( state != currentState && checkState( state, img ) ){
                changeState( state );
            }
            if( !checkState( currentState, img ) ){
                currentState = unknownState;
            }
        }
    }

    private void changeState( State newState ){
        currentState.leave( newState );
        newState.enter( currentState );
        currentState = newState;
    }

    private boolean checkState( State state, BufferedImage img ){
        if( !(img == null) ){
            Set<Entry<Point, Color>> entries = state.getIdentifiers().entrySet();
            boolean match = true;
            for( Entry<Point, Color> entry : entries ){
                Point p = entry.getKey();
                int screenRGB = img.getRGB( p.x, p.y );
                int stateRGB = entry.getValue().getRGB();
                match &= compareColor( screenRGB, stateRGB );
            }
            return match;
        }
        return false;
    }
}
