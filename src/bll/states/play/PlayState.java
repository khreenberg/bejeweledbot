package bll.states.play;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.awt.event.InputEvent;
import java.util.HashMap;

import bll.states.State;
import bll.states.play.MatchManager.Move;

public class PlayState extends State{

    private static final int MOVE_DELAY = 5;
    private static final int SWAP_DELAY = 0; // Use 400 for the "No invalid moves"-challenge

    private MatchManager matcher;
    private BoardReader boardReader;

    public PlayState() {
        super( "PlayState" );
        try{
            matcher = MatchManager.getInstance();
            boardReader = BoardReader.getInstance();
        }
        catch( AWTException e ){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void act(){
        boardReader.updateBoard( scrReader.getAppScreen() );
        makeSwap();
        bot.delay( SWAP_DELAY );
    }

    private void makeSwap(){
        //TODO First try and get multipliers every time
        if( !boardReader.isBoardMoving() ){
            Move move = matcher.getBestMove( matcher.getAllMoves( boardReader.getBoard() ) );
            if( move != null ){
                String str = move.toString();
                if( !str.contains( "THREE" ) ) System.out.println( "Moving: " + move );
                makeMove( move );
            }
        }
    }

    private void makeMove( Move move ){
        Point p = move.getGemCell();
        int row = p.x, col = p.y;
        moveMouseToCell( row, col );
        bot.delay( MOVE_DELAY );
        bot.mousePress( InputEvent.BUTTON1_MASK );
//        bot.delay( MOVE_DELAY );

        switch( move.getDirection() ){
            case LEFT:
                moveMouseToCell( row, col - 1 );
                break;
            case RIGHT:
                moveMouseToCell( row, col + 1 );
                break;
            case UP:
                moveMouseToCell( row - 1, col );
                break;
            case DOWN:
                moveMouseToCell( row + 1, col );
                break;
        }

        bot.delay( MOVE_DELAY );
        bot.mouseRelease( InputEvent.BUTTON1_MASK );
        bot.delay( MOVE_DELAY );
    }

    private void moveMouseToCell( int row, int col ){
        if( row >= BoardReader.NUM_ROWS || col >= BoardReader.NUM_COLUMNS ){
            throw new IllegalArgumentException( "Value out of range." );
        }
        Point newCoord = boardReader.getMiddleOfCellOnScreen( row, col );
        bot.moveMouse( newCoord.x, newCoord.y );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){
            {
                put( new Point( 53, 400 ), new Color( 0xdb7d38 ) );
                put( new Point( 120, 420 ), new Color( 0xcb7333 ) );
            }
        };
    }
}
