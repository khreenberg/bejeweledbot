package bll.states.play;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class TimeUpState extends State{

    public TimeUpState( ) {
        super( "TimeUpState" );
    }

    @Override
    public void act(){
        // Do nothing
    }

    @Override
    public void enter(State previousState) {
        if( previousState instanceof PlayState ) bot.delay( 4000 );
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){{
            put( new Point( 218, 265 ), new Color( 0x619391 ));
            put( new Point( 333, 250 ), new Color( 0x8affc8 ));
            
        }};
    }
}
