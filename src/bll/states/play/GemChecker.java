package bll.states.play;

import java.awt.Color;

import static bll.states.play.Gem.*;
import static tools.ColorCompare.*;

public class GemChecker{

    private static final int GEM_TOLERANCE = 2;

    private static boolean checkColors( Color selected, Gem...gems ){
        for( Gem g : gems )
            for( Color shade : g.getShades() )
                if( compareColor( selected, shade, GEM_TOLERANCE ) ) return true;
        return false;
    }

    /* Checks */
    public static boolean isYellow( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, YELLOWS );
    }

    public static boolean isPurple( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, PURPLES );
    }

    public static boolean isGreen( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, GREENS );
    }

    public static boolean isBlue( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, BLUES );
    }

    public static boolean isOrange( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, ORANGES );
    }

    public static boolean isRed( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, REDS );
    }

    public static boolean isWhite( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, WHITES );
    }

    public static boolean isHyperCube( int red, int green, int blue ){
        Color gem = new Color( red, green, blue );
        return checkColors( gem, HYPERCUBE );
    }
}
