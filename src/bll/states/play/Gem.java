package bll.states.play;

import java.awt.Color;
import java.util.ArrayList;

enum GemCategory {
    BLUE,
    GREEN,
    ORANGE,
    PURPLE,
    RED,
    YELLOW,
    WHITE,
    HYPERCUBE;
}

// @foff
public enum Gem {
    HYPERCUBE_GEM (new int[][]{
        {146, 114, 88},
        {121, 208, 114},
        {167, 238, 163},
        {204, 185, 140},
        {138, 206, 135}
    }),

    BLUE_NORMAL (new int[][]{
        { 38, 225, 254},
        { 41, 221, 251}  // Below purple star
    }),
    BLUE_FIRE (new int[][]{
        { 40, 226, 254}
    }),
    BLUE_STAR (new int[][]{
        { 60, 246, 255},
        { 68, 255, 255}
    }),
    BLUE_MULTI (new int[][]{
        {111, 168, 218}, // x2
        {110, 167, 217}, // x3
        {130, 197, 255}  // x6
    }),

    GREEN_NORMAL (new int[][]{
        { 84, 254, 128},
        { 89, 241, 131}
    }),
    GREEN_FIRE (new int[][]{
        { 92, 255, 132}
    }),
    GREEN_STAR (new int[][]{
        {159, 255, 250},
        {182, 255, 255},
        {112, 255, 179}
    }),
    GREEN_MULTI (new int[][]{
        { 83, 194, 89}, // x2
        { 98, 228, 105} // x4
    }),
    
    ORANGE_NORMAL (new int[][]{
        {254, 236, 120},
        {233, 222, 127}, // Below red star
        {255, 236, 120}
    }),
    ORANGE_FIRE (new int[][]{
        {255, 237, 127}
    }),
    ORANGE_STAR (new int[][]{
        {240, 234, 142},
        {255, 255, 212}
    }),
    ORANGE_MULTI (new int[][]{
        {218, 154, 111}, // x2
        {255, 181, 130}  // x4
    }),

    PURPLE_NORMAL (new int[][]{
        {221,  58, 236}, // Below red star
        {243,  43, 245}, // Below white star
        {251,  43, 251}
    }),
    PURPLE_FIRE (new int[][]{
        {252,  64, 251}
    }),
    PURPLE_STAR (new int[][]{
        {219,  63, 236},
        {233,  79, 255},
        {246,  96, 255}
    }),
    PURPLE_MULTI (new int[][]{
        {199, 100, 199}, // x2
        {226, 133, 228}  // x3
    }),

    RED_NORMAL (new int[][]{
        {243,  43,  75},
        {210,  58,  92}, // Below green star
        {215,  58,  92}
    }),
    RED_FIRE (new int[][]{
        {244,  50,  80}
    }),
    RED_STAR (new int[][]{
        {245,  74, 114},
        {255, 140, 190},
        {255, 101, 146},
        {253, 84, 126}
    }),
    RED_MULTI (new int[][]{
        {218,  96, 106}, // x2 (Very much like 'x3')
        {217,  96, 106}, // x3
        {255, 113, 125}, // x4
        {249, 132, 137}, // x5
        {222,  98, 108}  // x7
    }),

    WHITE_NORMAL (new int[][]{
        {253, 253, 253},
        {254, 254, 254}
    }),
    WHITE_FIRE (new int[][]{
        {255, 255, 254}
    }),
    WHITE_STAR (new int[][]{
        {225, 232, 240},
        {255, 255, 255}
    }),
    WHITE_MULTI (new int[][]{
        {200, 200, 200}, // x3
        {232, 232, 232}, // x5
        {235, 235, 235}  // x6
    }),
    
    YELLOW_NORMAL (new int[][]{
        {251, 241,  71},
        {253, 253,  74},
        {254, 254,  75}
    }),
    YELLOW_COIN (new int[][]{
        {236, 213,  46}
    }),
    YELLOW_FIRE (new int[][] {
        {255, 255,  88},
    }),
    YELLOW_STAR (new int[][]{
        {255, 255, 165}
    }),
    YELLOW_MULTI (new int[][]{
        {218, 216,  82}, // x2
        {217, 215,  81}, // x3
        {255, 253,  96}  // x4
    });

    // @fon
    public static Gem[] HYPERCUBE = { HYPERCUBE_GEM };
    public static Gem[] BLUES = { BLUE_NORMAL, BLUE_FIRE, BLUE_STAR, BLUE_MULTI };
    public static Gem[] GREENS = { GREEN_NORMAL, GREEN_FIRE, GREEN_STAR, GREEN_MULTI };
    public static Gem[] ORANGES = { ORANGE_NORMAL, ORANGE_FIRE, ORANGE_STAR, ORANGE_MULTI };
    public static Gem[] PURPLES = { PURPLE_NORMAL, PURPLE_FIRE, PURPLE_STAR, PURPLE_MULTI };
    public static Gem[] REDS = { RED_NORMAL, RED_FIRE, RED_STAR, RED_MULTI };
    public static Gem[] WHITES = { WHITE_NORMAL, WHITE_FIRE, WHITE_STAR, WHITE_MULTI };
    public static Gem[] YELLOWS = { YELLOW_NORMAL, YELLOW_COIN, YELLOW_STAR, YELLOW_MULTI };

    private Color[] shades;

    private Gem( int[][] colors ) {
        if( colors[0].length != 3 ) throw new RuntimeException( "Error in Gem color matrix!" );
        ArrayList<Color> colorList = new ArrayList<>();
        for( int[] rgb : colors )
            colorList.add( new Color( rgb[0], rgb[1], rgb[2] ) );
        shades = colorList.toArray( new Color[colorList.size()] );
    }

    public Color[] getShades(){
        return shades;
    }

    public static Gem[] getGemSet( GemCategory color ){
        switch( color ){
            case BLUE:
                return BLUES;
            case GREEN:
                return GREENS;
            case RED:
                return REDS;
            case PURPLE:
                return PURPLES;
            case ORANGE:
                return ORANGES;
            case YELLOW:
                return YELLOWS;
            case WHITE:
                return WHITES;
            default:
                return new Gem[0];
        }
    }
}
