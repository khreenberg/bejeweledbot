package bll.states.play;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import bll.states.play.MatchManager.Move.Direction;

public class MatchManager{

    private enum Pattern {
        THREE,
        FOUR,
        FIVE,
        STAR,
        HYPERCUBE,
        NONE, ;
    }

    private static MatchManager instance;

    private MatchManager() {
    }

    public static MatchManager getInstance(){
        if( instance == null ) instance = new MatchManager();
        return instance;
    }

    public static class Move implements Comparable<Move>{

        public static enum Direction {
            LEFT,
            RIGHT,
            UP,
            DOWN;
        }

        Point gemPosition;
        Direction direction;
        Pattern pattern;
        int priority;

        final int THREE_PRIORITY = 1;
        final int FOUR_PRIORITY = 2;
        final int STAR_PRIORITY = 3;
        final int FIVE_PRIORITY = 4;

        private Move( Point gemPosition, Direction direction, Pattern pattern, boolean destroysMultiplier ) {
            this.gemPosition = gemPosition;
            this.direction = direction;
            this.pattern = pattern;
            this.priority = calculatePriority( destroysMultiplier );
        }

        private Move( Point hypercubePosition, Direction direction, int gemsDestroyed, boolean destroysMultiplier ) {
            this( hypercubePosition, direction, Pattern.HYPERCUBE, destroysMultiplier );
            this.priority = gemsDestroyed * (destroysMultiplier ? 10 : 1);
        }

        public Point getGemCell(){
            return gemPosition;
        }

        public Direction getDirection(){
            return direction;
        }

        @SuppressWarnings( "incomplete-switch" )
        private int calculatePriority( boolean destroysMultiplier ){
            int priority = 0;
            switch( pattern ){
                case THREE:
                    priority = THREE_PRIORITY;
                    break;
                case FOUR:
                    priority = FOUR_PRIORITY;
                    break;
                case FIVE:
                    priority = FIVE_PRIORITY;
                    break;
                case STAR:
                    priority = STAR_PRIORITY;
                    break;
            }
            return priority * (destroysMultiplier ? 10 : 1);
        }

        @Override
        public int compareTo( Move move ){
            if( this.priority < move.priority ) return 1;
            if( this.priority > move.priority ) return -1;
            return 0;
        }

        @Override
        public String toString(){
            String dir;
            switch( direction ){
                case LEFT:
                    dir = "to the left";
                    break;
                case RIGHT:
                    dir = "to the right";
                    break;
                case UP:
                    dir = "up";
                    break;
                case DOWN:
                    dir = "down";
                    break;
                default:
                    dir = "somewhere";
                    break;
            }
            return String.format( "Move (%d,%d) %s to create a %s. (Priority = %d)", gemPosition.x + 1, gemPosition.y + 1, dir, pattern,  priority );
        }
    }

    public Move getBestMove( final Move[] moves ){
        if( moves == null || moves.length < 1 ) return null;
        List<Move> movesAsList = new ArrayList<Move>(){
            {
                for( Move m : moves )
                    add( m );
            }
        };
        Collections.sort( movesAsList );
        return movesAsList.get( 0 );
    }

    public Move[] getAllMoves( char[][] board ){
        ArrayList<Move> possibleMoves = new ArrayList<>();
        for( int r = 0; r < board.length; ++r ){
            for( int c = 0; c < board[r].length; ++c ){
                possibleMoves.addAll( getCellMoves( r, c, board ) );
            }
        }
        return possibleMoves.toArray( new Move[possibleMoves.size()] );
    }

    private Point[] getPotentialPatternCells( int row, int col, char[][] board ){
        ArrayList<Point> cells = new ArrayList<>();
        floodFill( row, col, board[row][col], board, cells );
        if( cells.size() < 3 ) return null;
        return cells.toArray( new Point[cells.size()] );
    }

    private Pattern checkPattern( Point[] cells ){
        if( cells != null && cells.length >= 3 ){
            if( isLine( 5, cells ) ) return Pattern.FIVE;
            if( isStar( cells ) ) return Pattern.STAR;
            if( isLine( 4, cells ) ) return Pattern.FOUR;
            if( isLine( 3, cells ) ) return Pattern.THREE;
        }
        return Pattern.NONE;
    }

    private boolean isLine( int lineLength, Point[] points ){
        if( points.length < 1 || points == null ) return false;
        for( Point p : points ){
            int[] matches = countMatches( p, points );
            if( matches[0] == lineLength || matches[1] == lineLength ) return true;
        }
        return false;
    }

    private boolean isStar( Point[] points ){
        for( Point p : points ){
            int[] matches = countMatches( p, points );
            if( matches[0] >= 3 && matches[1] >= 3 ) return true;
        }
        return false;
    }

    /**
     * Counts the common x & y values for a given point and returns it as an array of ints with result[0] =
     * number of vertical matches (same x-value) and result[1] = number of horizontal matches (same y-value).
     * 
     * @param p Point to test against
     * @param points collection of points
     * @return an int[2] with [0] = vertical matches and [1] = horizontal matches.
     */
    private int[] countMatches( Point p, Point[] points ){
        int[] result = new int[2];
        for( Point q : points ){
            result[0] += p.x == q.x ? 1 : 0;
            result[1] += p.y == q.y ? 1 : 0;
        }
        return result;
    }

    private void floodFill( int row, int col, char color, char[][] board, ArrayList<Point> storage ){
        Point cell = new Point( row, col );
        if( board[row][col] != color || storage.contains( cell ) ) return;

        storage.add( cell );
        if( row != 0 ) floodFill( row - 1, col, color, board, storage );
        if( col != 0 ) floodFill( row, col - 1, color, board, storage );
        if( row != board.length - 1 ) floodFill( row + 1, col, color, board, storage );
        if( col != board[row].length - 1 ) floodFill( row, col + 1, color, board, storage );
    }

    private Pattern swapAndCheck( int r1, int c1, int r2, int c2, char[][] board ){
        char color = board[r1][c1];
        char swapColor = board[r2][c2];
        Pattern p = Pattern.NONE;
        if( swapColor != color ){
            board[r1][c1] = swapColor;
            board[r2][c2] = color;
            p = checkPattern( getPotentialPatternCells( r2, c2, board ) );
            board[r1][c1] = color;
            board[r2][c2] = swapColor;
        }
        return p;
    }

    public ArrayList<Move> getCellMoves( int row, int col, char[][] board ){
        char color = board[row][col];
        Point position = new Point( row, col );
        ArrayList<Move> potentialMoves = new ArrayList<>();

        if( color == '?' ){
            return potentialMoves;
        }else if( color == 'H' ){
            // TODO Put better decisionmaking here
            if( col != 0 ) potentialMoves.add( new Move( position, Direction.LEFT, Pattern.HYPERCUBE, false ) );
            if( row != 0 ) potentialMoves.add( new Move( position, Direction.UP, Pattern.HYPERCUBE, false ) );
            if( col != BoardReader.NUM_COLUMNS - 1 ) potentialMoves.add( new Move( position,
                                                                                    Direction.RIGHT,
                                                                                    Pattern.HYPERCUBE,
                                                                                    false ) );
            if( row != BoardReader.NUM_ROWS - 1 ) potentialMoves.add( new Move( position,
                                                                                 Direction.DOWN,
                                                                                 Pattern.HYPERCUBE,
                                                                                 false ) );
        }
        if( col != 0 ){
            Pattern pattern = swapAndCheck( row, col, row, col - 1, board );
            if( pattern != Pattern.NONE ) potentialMoves.add( new Move( position, Direction.LEFT, pattern, false ) );
        }

        if( row != 0 ){
            Pattern pattern = swapAndCheck( row, col, row - 1, col, board );
            if( pattern != Pattern.NONE ) potentialMoves.add( new Move( position, Direction.UP, pattern, false ) );
        }

        if( col != BoardReader.NUM_COLUMNS - 1 ){
            Pattern pattern = swapAndCheck( row, col, row, col + 1, board );
            if( pattern != Pattern.NONE ) potentialMoves.add( new Move( position, Direction.RIGHT, pattern, false ) );
        }

        if( row != BoardReader.NUM_ROWS - 1 ){
            Pattern pattern = swapAndCheck( row, col, row + 1, col, board );
            if( pattern != Pattern.NONE ) potentialMoves.add( new Move( position, Direction.DOWN, pattern, false ) );
        }

        return potentialMoves;
    }
}
