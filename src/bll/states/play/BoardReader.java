package bll.states.play;

import static bll.states.play.GemChecker.*;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import dal.ScreenReader;

public class BoardReader{

    private char[][] currentBoard;
    private char[][] previousBoard;
    private boolean isBoardMoving = false;
    private final int BOARD_MOVE_TOLERANCE = 3;
    private final int BOARD_UPDATE_DELAY = 15;
    private long lastUpdateTime;

    private static int boardX;
    private static int boardY;

    private static final int ID_X_FRACTION = 2;
    private static final int ID_Y_FRACTION = 4;

    private static final int BOARD_X_OFFSET = 175;
    private static final int BOARD_Y_OFFSET = 109;

    public static final int CELL_WIDTH = 40;
    public static final int CELL_HEIGHT = 40;
    public static final int NUM_ROWS = 8;
    public static final int NUM_COLUMNS = 8;

    private ScreenReader scrReader;

    private static BoardReader instance;

    private BoardReader() throws AWTException {
        scrReader = ScreenReader.getInstance();
        Rectangle appBounds = scrReader.getAppBounds();
        boardX = (int) (appBounds.getX() + BOARD_X_OFFSET);
        boardY = (int) (appBounds.getY() + BOARD_Y_OFFSET);
        currentBoard = new char[NUM_ROWS][NUM_COLUMNS];
        previousBoard = currentBoard.clone();
        lastUpdateTime = System.currentTimeMillis();
    }

    public static BoardReader getInstance() throws AWTException{
        if( instance == null ) instance = new BoardReader();
        return instance;
    }

    public void updateBoard( BufferedImage appImage ){
        long time = System.currentTimeMillis();
        if( time - lastUpdateTime > BOARD_UPDATE_DELAY ){
            previousBoard = deepCloneBoard( currentBoard );
            for( int col = 0; col < NUM_COLUMNS; col++ ){
                for( int row = 0; row < NUM_ROWS; row++ ){
                    currentBoard[row][col] = calcCellChar( row, col, appImage );
                }
            }
            lastUpdateTime = time;
        }
        isBoardMoving = !compareBoards( currentBoard, previousBoard, BOARD_MOVE_TOLERANCE );
    }

    public static char[][] deepCloneBoard( char[][] board ){
        if( board == null ) return null;
        char[][] clone = new char[board.length][];
        for( int i = 0; i < board.length; i++ ){
            clone[i] = new char[board[i].length];
            for( int j = 0; j < board[i].length; j++ ){
                clone[i][j] = board[i][j];
            }
        }
        return clone;
    }

    public static boolean compareBoards( char[][] b1, char[][] b2, int tolerance ){
        int counter = 0;
        if( b1.length != b2.length ) throw new RuntimeException( "Cannot compare boards!" );
        for( int r = 0; r < b1.length; ++r ){
            for( int c = 0; c < b1[r].length; ++c ){
                if( b1[r][c] != b2[r][c] ){
                    counter++;
                    if( counter > tolerance ) return false;
                }
            }
        }
        return true;
    }

    public boolean isBoardMoving(){
        return false;
//        return isBoardMoving;
    }

    public Point getCellIdentifier( int row, int col ){
        Point coords = new Point();
        coords.x = BOARD_X_OFFSET + col * CELL_WIDTH + CELL_WIDTH / ID_X_FRACTION;
        coords.y = BOARD_Y_OFFSET + row * CELL_HEIGHT + CELL_HEIGHT / ID_Y_FRACTION;
        return coords;
    }

    public char[][] getBoard(){
        return currentBoard;
    }

    private Point getMiddleOfCell( int row, int col ){
        Point coords = new Point();
        coords.x = col * CELL_WIDTH + CELL_WIDTH / 2;
        coords.y = row * CELL_HEIGHT + CELL_HEIGHT / 2;
        return coords;
    }

    public Point getMiddleOfCellOnScreen( int row, int col ){
        Point coords = getMiddleOfCell( row, col );
        coords.x += boardX;
        coords.y += boardY;
        return coords;
    }

    private char calcCellChar( int row, int col, BufferedImage appImage ){
        Point point = getCellIdentifier( row, col );
        int pixel = appImage.getRGB( point.x, point.y );
        int red = (pixel >> 16) & 0xff;
        int green = (pixel >> 8) & 0xff;
        int blue = pixel & 0xff;

        if( isYellow( red, green, blue ) ){
            return 'Y';
        }else if( isPurple( red, green, blue ) ){
            return 'P';
        }else if( isGreen( red, green, blue ) ){
            return 'G';
        }else if( isBlue( red, green, blue ) ){
            return 'B';
        }else if( isOrange( red, green, blue ) ){
            return 'O';
        }else if( isRed( red, green, blue ) ){
            return 'R';
        }else if( isWhite( red, green, blue ) ){
            return 'W';
        }else if( isHyperCube( red, green, blue ) ){
            return 'H';
        }else{
            return '?';
        }
    }

    /* Development */
    public HashMap<Point, Color> getIdentifiers(){
        HashMap<Point, Color> map = new HashMap<>();
        BufferedImage img = scrReader.getAppScreen();
        Point p;
        for( int r = 0; r < NUM_ROWS; ++r ){
            for( int c = 0; c < NUM_COLUMNS; ++c ){
                p = getCellIdentifier( r, c );
                map.put( new Point( r + 1, c + 1 ), new Color( img.getRGB( p.x, p.y ) ) );
            }
        }
        return map;
    }
}
