package bll.states.play;

import java.awt.Color;
import java.awt.Point;
import java.util.HashMap;

import bll.states.State;

public class SpecialGemState extends State{

    public SpecialGemState() {
        super( "SpecialGemState" );
    }

    @Override
    public void act(){
    }

    @Override
    public void setStateIdentifiers(){
        stateIdentifiers = new HashMap<Point, Color>(){
            {
                put( new Point( 53, 400 ), new Color( 0x70401c ) );
                put( new Point( 120, 420 ), new Color( 0x683b1a ) );
            }
        };
    }
}
