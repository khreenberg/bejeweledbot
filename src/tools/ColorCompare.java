package tools;

import java.awt.Color;

public class ColorCompare{

    private static final int DEFAULT_TOLERANCE = 10;

    public static boolean compareColor( Color c1, Color c2 ){
        return compareColor( c1.getRGB(), c2.getRGB(), DEFAULT_TOLERANCE );
    }

    public static boolean compareColor( int rgb1, int rgb2 ){
        return compareColor( rgb1, rgb2, DEFAULT_TOLERANCE );
    }

    public static boolean compareColor( Color c1, Color c2, int tolerance ){
        return compareColor( c1.getRGB(), c2.getRGB(), tolerance );
    }

    // @foff (Disables code-formatting in Eclipse)
    public static boolean compareColor( int rgb1, int rgb2, int tolerance ){
        int c1R = (rgb1 >> 16) & 0xFF;
        int c2R = (rgb2 >> 16) & 0xFF;
        int c1G = (rgb1 >> 8) & 0xFF;
        int c2G = (rgb2 >> 8) & 0xFF;
        int c1B = (rgb1 >> 0) & 0xFF;
        int c2B = (rgb2 >> 0) & 0xFF;
        return Math.abs( c1R - c2R ) < tolerance 
            && Math.abs( c1G - c2G ) < tolerance
            && Math.abs( c1B - c2B ) < tolerance;
    }
}
