package tools;

import java.awt.Color;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import bll.states.play.Gem;


public class ProjectTools{

    public static boolean printBoard( char[][] board ){
        boolean allIdentified = true;
        if( board != null )
            for( int r = 0; r < board.length; ++r ){
                for( int c = 0; c < board[r].length; ++c ){
                    System.out.print( board[r][c] + " " );
                    allIdentified &= board[r][c] != '?';
                }
                System.out.println();
            }
        else System.out.println( "Board is null!" );
        return allIdentified;
    }

    public static <K, V> Set<K> getKeysByValue( Map<K, V> map, V value ){
        Set<K> keys = new HashSet<K>();
        for( Entry<K, V> entry : map.entrySet() ){
            if( value.equals( entry.getValue() ) ){
                keys.add( entry.getKey() );
            }
        }
        return keys;
    }

    public static boolean isColorRecorded( Color c ){
        for( Gem g : Gem.values() ){
            for( Color shade : g.getShades() )
                if( shade.getRGB() == c.getRGB() ) return true;
        }
        return false;
    }
}
