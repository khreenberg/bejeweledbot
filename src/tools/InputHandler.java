package tools;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;
import org.jnativehook.mouse.NativeMouseEvent;
import org.jnativehook.mouse.NativeMouseListener;

public class InputHandler implements NativeKeyListener, NativeMouseListener{

    private volatile boolean[] keys = new boolean[256];
    private volatile boolean[] buttons = new boolean[6];

    private static InputHandler instance;

    private InputHandler() {
    }

    public static InputHandler getInstance(){
        if( instance == null ) instance = new InputHandler();
        return instance;
    }

    public boolean isKeyDown( int keyCode ){
        if( 0 < keyCode && keyCode < 256 ){
            return keys[keyCode];
        }
        return false;
    }

    public boolean isButtonDown( int mouseButton ){
        if( 0 < mouseButton && mouseButton < 6 ){
            return buttons[mouseButton];
        }
        return false;
    }

    @Override
    public void nativeMouseClicked( NativeMouseEvent e ){
    }

    @Override
    public void nativeMousePressed( NativeMouseEvent e ){
        if( 0 <= e.getButton() && e.getButton() < 6 ){
            buttons[e.getButton()] = true;
        }
    }

    @Override
    public void nativeMouseReleased( NativeMouseEvent e ){
        if( 0 <= e.getButton() && e.getButton() < 6 ){
            buttons[e.getButton()] = false;
        }
    }

    @Override
    public void nativeKeyPressed( NativeKeyEvent e ){
        if( 0 < e.getKeyCode() && e.getKeyCode() < 256 ){
            keys[e.getKeyCode()] = true;
        }
    }

    @Override
    public void nativeKeyReleased( NativeKeyEvent e ){
        if( 0 < e.getKeyCode() && e.getKeyCode() < 256 ){
            keys[e.getKeyCode()] = false;
        }
    }

    @Override
    public void nativeKeyTyped( NativeKeyEvent e ){
    }
}
