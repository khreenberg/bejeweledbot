package app;

import static org.jnativehook.keyboard.NativeKeyEvent.*;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.*;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;

import org.jnativehook.GlobalScreen;
import org.jnativehook.mouse.NativeMouseEvent;

import tools.InputHandler;
import bll.BejeweledBot;
import dal.ScreenReader;

public class MainWindow extends JFrame{

    private enum AppState {
        PASSIVE,
        AWAITING_INIT,
        INITIALIZED,
        RUNNING,
        STOPPED
    }

    private static volatile AppState currentAppState = AppState.PASSIVE;

    private static Thread botThread;
    private static Thread inputThread;
    private static Thread screenUpdateThread;

    private static BejeweledBot bot;
    private static ScreenReader scrReader;

    private static InputHandler input;
    private static final int INPUT_DELAY = 200; // milliseconds
    private static long lastInputTime;

    private static volatile boolean stopped = false;
    private static volatile boolean paused = true;

    public static void main( String[] args ){
        try{
            input = InputHandler.getInstance();
            scrReader = ScreenReader.getInstance();

            GlobalScreen.registerNativeHook();
            GlobalScreen.getInstance().addNativeKeyListener( input );
            GlobalScreen.getInstance().addNativeMouseListener( input );

            UIManager.setLookAndFeel( UIManager.getSystemLookAndFeelClassName() );
            createThreads();
            lastInputTime = System.currentTimeMillis();
            EventQueue.invokeLater( new Runnable(){
                @Override
                public void run(){
                    try{
                        MainWindow frame = new MainWindow();
                        frame.setVisible( true );
                    }
                    catch( Exception e ){
                        e.printStackTrace();
                    }
                }
            } );
        }
        catch( Exception e ){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private static void createThreads(){
        /* Input Thread */
        inputThread = new Thread( new Runnable(){
            @Override
            public void run(){
                while( true ){
                    if( System.currentTimeMillis() - lastInputTime > INPUT_DELAY ) handleInput();
                }
            }
        } );
        inputThread.start();
        /* Bot Thread */
        botThread = new Thread( new Runnable(){
            @Override
            public void run(){
                while( !stopped ){
                    if( !paused ) bot.update();
                }
            }
        } );
        botThread.start();
        /* Screen Update Thread */
        screenUpdateThread = new Thread( new Runnable(){
            @Override
            public void run(){
                while( !stopped ){
                    if( !paused ) scrReader.updateScreen();
                }
            }
        } );
        screenUpdateThread.start();
    }

    private static void initializeLogic(){
        scrReader.initializeOrigin();
        bot = BejeweledBot.getInstance();
    }

    private static void handleInput(){
        boolean inputIsHandled = false;
        do{
            if( currentAppState == AppState.AWAITING_INIT ){
                if( input.isKeyDown( VK_CONTROL ) && input.isButtonDown( NativeMouseEvent.BUTTON1 ) ){
                    inputIsHandled = true;
                    println( "Checkpoint" );
                    initializeLogic();
                    enterInitialized();
                    lastInputTime = System.currentTimeMillis();
                }else if( input.isKeyDown( VK_ESCAPE ) ){
                    enterPassive();
                    inputIsHandled = true;
                }
            }else if( currentAppState == AppState.RUNNING ){
                if( input.isKeyDown( VK_ESCAPE ) ){
                    btnStopPressed();
                }
            }
        }
        while( !inputIsHandled );
    }

    private static void enterPassive(){
        btnReinitialize.setEnabled( false );
        btnBotMainControl.setText( "Ready" );
        currentAppState = AppState.PASSIVE;
    }

    private static void enterAwaitingInit(){
        btnReinitialize.setEnabled( false );
        btnBotMainControl.setText( "Cancel" );
        currentAppState = AppState.AWAITING_INIT;
    }

    private static void enterInitialized(){
        btnReinitialize.setEnabled( true );
        btnBotMainControl.setText( "Start" );
        currentAppState = AppState.INITIALIZED;
    }

    private static void enterRunning(){
        paused = false;
        btnReinitialize.setEnabled( true );
        btnBotMainControl.setText( "Stop" );
        currentAppState = AppState.RUNNING;
    }

    private static void enterStopped(){
        paused = true;
        btnReinitialize.setEnabled( true );
        btnBotMainControl.setText( "Start" );
        currentAppState = AppState.STOPPED;
    }

    private void btnBotMainControlPressed(){
        switch( currentAppState ){
            case PASSIVE:
                btnReadyPressed();
                break;
            case AWAITING_INIT:
                btnCancelPressed();
                break;
            case INITIALIZED:
                btnStartPressed();
                break;
            case RUNNING:
                btnStopPressed();
                break;
            case STOPPED:
                btnStartPressed();
                break;
            default:
                break;
        }
    }

    private void btnReadyPressed(){
        enterAwaitingInit();
    }

    private void btnCancelPressed(){
        enterPassive();
    }

    private void btnStartPressed(){
        enterRunning();
    }

    private static void btnStopPressed(){
        enterStopped();
    }

    private static void println( String str ){
        try{
            StyleContext sc = StyleContext.getDefaultStyleContext();
            AttributeSet aset = sc.addAttribute( SimpleAttributeSet.EMPTY, StyleConstants.Foreground, Color.RED );

            aset = sc.addAttribute( aset, StyleConstants.FontFamily, "Lucida Console" );
            aset = sc.addAttribute( aset, StyleConstants.Alignment, StyleConstants.ALIGN_JUSTIFIED );

            txtpnConsoleText.setCharacterAttributes( aset, false );
            Document doc = txtpnConsoleText.getDocument();
            doc.insertString( doc.getLength(), str + "\n", aset );
        }
        catch( BadLocationException e ){
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // @foff
    private static JPanel contentPane;
    private static JScrollPane pnlConsole;
    private static JTextPane txtpnConsoleText;
    private static JPanel pnlMessage;
    private static JPanel pnlBotControl;
    private static JMenuBar menuBar;
    private static JButton btnBotMainControl;
    private static JButton btnAdvancedOptions;
    private static JCheckBox chckbxPrintMove;
    private static JButton btnReinitialize;
    private static JPanel pnlOptions;

    public MainWindow() {
        setMinimumSize( new Dimension( 345, 365 ) );
        setTitle( "BejeweledBot" );
        setBounds( 100, 100, 345, 365 );
        setDefaultCloseOperation( JFrame.DO_NOTHING_ON_CLOSE );
        addWindowListener( new WindowAdapter(){
            @Override
            public void windowClosing( WindowEvent e ){
                GlobalScreen.unregisterNativeHook();
                stopped = true;
                System.exit( 0 );
            }
        } );

        menuBar = new JMenuBar();
        setJMenuBar( menuBar );
        contentPane = new JPanel();
        contentPane.setBorder( new EmptyBorder( 5, 5, 5, 5 ) );
        setContentPane( contentPane );

        pnlConsole = new JScrollPane();
        pnlConsole.setBorder( new CompoundBorder( new TitledBorder( UIManager.getBorder( "TitledBorder.border" ),
                                                                    "Console",
                                                                    TitledBorder.LEADING,
                                                                    TitledBorder.TOP,
                                                                    null,
                                                                    null ), new BevelBorder( BevelBorder.LOWERED,
                                                                                             null,
                                                                                             null,
                                                                                             null,
                                                                                             null ) ) );

        pnlMessage = new JPanel();
        pnlMessage.setBorder( new TitledBorder( null, "Messages", TitledBorder.LEADING, TitledBorder.TOP, null, null ) );

        pnlBotControl = new JPanel();
        pnlBotControl.setBorder( new TitledBorder( null,
                                                   "Bot controls",
                                                   TitledBorder.LEADING,
                                                   TitledBorder.TOP,
                                                   null,
                                                   null ) );

        pnlOptions = new JPanel();
        pnlOptions.setBorder( new TitledBorder( null, "Options", TitledBorder.LEADING, TitledBorder.TOP, null, null ) );
        GroupLayout gl_contentPane = new GroupLayout( contentPane );
        gl_contentPane.setHorizontalGroup( gl_contentPane.createParallelGroup( Alignment.LEADING )
                                           .addGroup( gl_contentPane.createSequentialGroup()
                                                      .addContainerGap()
                                                      .addComponent( pnlConsole, GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE )
                                                      .addPreferredGap( ComponentPlacement.RELATED )
                                                      .addGroup( gl_contentPane.createParallelGroup( Alignment.LEADING, false )
                                                                 .addComponent( pnlOptions, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE )
                                                                 .addGroup( gl_contentPane.createParallelGroup( Alignment.TRAILING )
                                                                            .addComponent( pnlMessage, Alignment.LEADING,GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE )
                                                                            .addComponent( pnlBotControl, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE ) ) )
                                                                            .addContainerGap() ) );
        gl_contentPane.setVerticalGroup( gl_contentPane.createParallelGroup( Alignment.LEADING )
                                         .addGroup( gl_contentPane.createSequentialGroup()
                                                    .addContainerGap()
                                                    .addGroup( gl_contentPane.createParallelGroup( Alignment.LEADING )
                                                               .addGroup( gl_contentPane.createSequentialGroup()
                                                                          .addComponent( pnlBotControl, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE )
                                                                          .addPreferredGap( ComponentPlacement.RELATED )
                                                                          .addComponent( pnlOptions, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE )
                                                                          .addPreferredGap( ComponentPlacement.RELATED )
                                                                          .addComponent( pnlMessage, GroupLayout.DEFAULT_SIZE, 94, Short.MAX_VALUE ) )
                                                                          .addComponent( pnlConsole, GroupLayout.DEFAULT_SIZE, 333, Short.MAX_VALUE ) )
                                                                          .addGap( 0 ) ) );

        btnAdvancedOptions = new JButton( "Advanced options..." );
        btnAdvancedOptions.setMargin( new Insets( 2, 7, 2, 7 ) );

        chckbxPrintMove = new JCheckBox( "Print move" );
        GroupLayout gl_pnlOptions = new GroupLayout( pnlOptions );
        gl_pnlOptions.setHorizontalGroup( gl_pnlOptions.createParallelGroup( Alignment.LEADING )
                                          .addGroup( gl_pnlOptions.createSequentialGroup()
                                                     .addContainerGap()
                                                     .addGroup( gl_pnlOptions.createParallelGroup( Alignment.LEADING )
                                                                .addComponent( btnAdvancedOptions, GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE )
                                                                .addComponent( chckbxPrintMove ) )
                                                                .addContainerGap() ) );
        gl_pnlOptions.setVerticalGroup( gl_pnlOptions.createParallelGroup( Alignment.LEADING )
                                        .addGroup( Alignment.TRAILING, gl_pnlOptions.createSequentialGroup()
                                                   .addContainerGap()
                                                   .addComponent( chckbxPrintMove )
                                                   .addPreferredGap( ComponentPlacement.RELATED, 38, Short.MAX_VALUE )
                                                   .addComponent( btnAdvancedOptions )
                                                   .addContainerGap() ) );
        pnlOptions.setLayout( gl_pnlOptions );

        btnBotMainControl = new JButton( "Ready" );
        btnBotMainControl.setMargin(new Insets(2, 7, 2, 7));
        btnBotMainControl.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                btnBotMainControlPressed();
            }
        });

        btnReinitialize = new JButton( "Reinitialize" );
        btnReinitialize.setEnabled(false);
        btnReinitialize.setMargin(new Insets(2, 7, 2, 7));
        GroupLayout gl_pnlBotControl = new GroupLayout( pnlBotControl );
        gl_pnlBotControl.setHorizontalGroup(
            gl_pnlBotControl.createParallelGroup(Alignment.TRAILING)
                .addGroup(gl_pnlBotControl.createSequentialGroup()
                    .addGap(6)
                    .addComponent(btnBotMainControl, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnReinitialize, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
        );
        gl_pnlBotControl.setVerticalGroup(
            gl_pnlBotControl.createParallelGroup(Alignment.LEADING)
                .addGroup(gl_pnlBotControl.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(gl_pnlBotControl.createParallelGroup(Alignment.BASELINE)
                        .addComponent(btnBotMainControl)
                        .addComponent(btnReinitialize))
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pnlBotControl.setLayout( gl_pnlBotControl );
        GroupLayout gl_pnlMessage = new GroupLayout( pnlMessage );
        gl_pnlMessage.setHorizontalGroup( gl_pnlMessage.createParallelGroup( Alignment.LEADING )
                                          .addGap( 0, 222, Short.MAX_VALUE ) );
        gl_pnlMessage.setVerticalGroup( gl_pnlMessage.createParallelGroup( Alignment.LEADING )
                                        .addGap( 0, 172, Short.MAX_VALUE ) );
        pnlMessage.setLayout( gl_pnlMessage );

        txtpnConsoleText = new JTextPane();
        txtpnConsoleText.setText( "Console text\n" );
        txtpnConsoleText.addKeyListener( new KeyAdapter(){ // HAHA! I'm a genius!
            @Override
            public void keyTyped( KeyEvent ke ){
                ke.consume();
            }
        } );
        ((DefaultCaret) txtpnConsoleText.getCaret()).setUpdatePolicy( DefaultCaret.ALWAYS_UPDATE );
        pnlConsole.setViewportView( txtpnConsoleText );
        contentPane.setLayout( gl_contentPane );
    }
}
