package dal;

import java.awt.AWTException;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

public class ScreenReader{

    private static final int SCREEN_UPDATE_DELAY = 75;
    private static final int APP_WIDTH = 760;
    private static final int APP_HEIGHT = 611;

    private Rectangle appBounds;
    private BufferedImage appImg;
    private Robot screenBot;

    private static ScreenReader instance;

    private ScreenReader() throws AWTException {
        screenBot = new Robot();
    }

    public static ScreenReader getInstance() throws AWTException{
        if( instance == null ) instance = new ScreenReader();
        return instance;
    }

    public void initializeOrigin(){
        Point mouse = MouseInfo.getPointerInfo().getLocation();
        Point origin = seekOrigin( mouse.x, mouse.y );
        appBounds = new Rectangle( origin.x, origin.y, APP_WIDTH, APP_HEIGHT );
    }

    public void updateScreen(){
        appImg = screenBot.createScreenCapture( appBounds );
        screenBot.delay( SCREEN_UPDATE_DELAY );
    }

    public BufferedImage getAppScreen(){
        return appImg;
    }

    public Rectangle getAppBounds(){
        return appBounds;
    }

    public Point convertAppPositionToScreen( Point p ){
        return new Point( p.x + (int) appBounds.getX(), p.y + (int) appBounds.getY() );
    }

    private Point seekOrigin( int x, int y ){
        BufferedImage img = screenBot.createScreenCapture( new Rectangle( Toolkit.getDefaultToolkit().getScreenSize() ) );
        int r, g, b;
        boolean isWithinApp;
        do{
            x--;
            int pixel = img.getRGB( x, y );
            r = (pixel >> 16) & 0xFF;
            g = (pixel >> 8) & 0xFF;
            b = (pixel >> 0) & 0xFF;
            isWithinApp = r < 255 && g < 255 && b < 255;
        }
        while( isWithinApp );
        x += 1;
        do{
            y--;
            int pixel = img.getRGB( x, y );
            r = (pixel >> 16) & 0xFF;
            g = (pixel >> 8) & 0xFF;
            b = (pixel >> 0) & 0xFF;
            isWithinApp = r < 255 && g < 255 && b < 255;
        }
        while( isWithinApp );
        y++;
        return new Point( x, y );
    }

    public void delay( int ms ){
        screenBot.delay( ms );
    }
}
